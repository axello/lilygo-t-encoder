# lilygo-t-encoder

A sample app for the LilyGo T-Encoder which colours the onboard LEDs in a rainbow fashion when rotating the encoder.

Based on and adapted from the 'base.ino' from [LilyGO](https://github.com/Xinyuan-LilyGO/T-Encoder)
