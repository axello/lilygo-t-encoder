#include "AiEsp32RotaryEncoder.h"
#include "Arduino.h"

/*
connecting Rotary encoder

Rotary encoder side    MICROCONTROLLER side  
-------------------    ---------------------------------------------------------------------
CLK (A pin)            any microcontroler intput pin with interrupt -> in this example pin 36
DT (B pin)             any microcontroler intput pin with interrupt -> in this example pin 37
SW (button pin)        any microcontroler intput pin with interrupt -> in this example pin 38
GND - to microcontroler GND
VCC                    microcontroler VCC (then set ROTARY_ENCODER_VCC_PIN -1) 

*/
#define LED_GREEN						25
#define LED_BLUE					26
#define LED_RED  					27
#define BUZZER 						19
#define ROTARY_ENCODER_A_PIN 		36
#define ROTARY_ENCODER_B_PIN    	37
#define ROTARY_ENCODER_BUTTON_PIN 	38
#define ROTARY_ENCODER_VCC_PIN 		-1 /* 27 put -1 of Rotary encoder Vcc is connected directly to 3,3V; else you can use declared output pin for powering rotary encoder */

//depending on your encoder - try 1,2 or 4 to get expected behaviour
//#define ROTARY_ENCODER_STEPS 1
#define ROTARY_ENCODER_STEPS 2
//#define ROTARY_ENCODER_STEPS 4

//instead of changing here, rather change numbers above
AiEsp32RotaryEncoder rotaryEncoder = AiEsp32RotaryEncoder(ROTARY_ENCODER_A_PIN, ROTARY_ENCODER_B_PIN, ROTARY_ENCODER_BUTTON_PIN, ROTARY_ENCODER_VCC_PIN, ROTARY_ENCODER_STEPS);

int16_t test_limits = 2;
int16_t hue = 0;

/* HSV to RGB conversion */

typedef struct {
    double r;       // ∈ [0, 1]
    double g;       // ∈ [0, 1]
    double b;       // ∈ [0, 1]
} rgb;

typedef struct {
    double h;       // ∈ [0, 360]
    double s;       // ∈ [0, 1]
    double v;       // ∈ [0, 1]
} hsv;

int led_red();
int led_green();
int led_blue();
int buzzer();
void setLedHue(double hue);
void setLedHSV(hsv HSV);

void rotary_loop();
void rotary_onButtonClick();


// use first channel of 16 channels (started from zero)
#define BUZZER_CHANNEL     0
#define LEDRED_CHANNEL     1
#define LEDGREEN_CHANNEL     2
#define LEDBLUE_CHANNEL     3

// use 13 bit precission for LEDC timer
#define LEDC_TIMER_13_BIT  13

// use 5000 Hz as a LEDC base frequency
#define LEDC_BASE_FREQ     2000

bool on_Button_down = false;


void setup() {
  Serial.begin(115200);

  // initialize digital pin output.
  pinMode(LED_RED, OUTPUT);
  pinMode(LED_GREEN, OUTPUT);
  pinMode(LED_BLUE, OUTPUT);
  pinMode(BUZZER, OUTPUT);

  ledcSetup(BUZZER_CHANNEL, LEDC_BASE_FREQ, LEDC_TIMER_13_BIT);
  ledcAttachPin(BUZZER, BUZZER_CHANNEL);

  ledcSetup(LEDRED_CHANNEL, LEDC_BASE_FREQ, 8);
  ledcAttachPin(LED_RED, LEDRED_CHANNEL);
  ledcSetup(LEDGREEN_CHANNEL, LEDC_BASE_FREQ, 8);
  ledcAttachPin(LED_GREEN, LEDGREEN_CHANNEL);
  ledcSetup(LEDBLUE_CHANNEL, LEDC_BASE_FREQ, 8);
  ledcAttachPin(LED_BLUE, LEDBLUE_CHANNEL);

  led_blue(); // blue led 

  //we must initialize rotary encoder
    rotaryEncoder.begin();

    rotaryEncoder.setup(
        [] { rotaryEncoder.readEncoder_ISR(); },
        [] { on_Button_down=true; });
    //optionally we can set boundaries and if values should cycle or not
    bool circleValues = true;
    rotaryEncoder.setBoundaries(0, 720, circleValues); //minValue, maxValue, circleValues true|false (when max go to min and vice versa)

    /*Rotary acceleration introduced 25.2.2021.
   * in case range to select is huge, for example - select a value between 0 and 1000 and we want 785
   * without accelerateion you need long time to get to that number
   * Using acceleration, faster you turn, faster will the value raise.
   * For fine tuning slow down.
   */
    //rotaryEncoder.disableAcceleration(); //acceleration is now enabled by default - disable if you dont need it
    rotaryEncoder.setAcceleration(250); //or set the value - larger number = more accelearation; 0 or 1 means disabled acceleration

}

// the loop function runs over and over again forever
void loop() {

//in loop call your custom function which will process rotary encoder values
    if (millis() > 20000)
        rotaryEncoder.enable();
 
    rotary_loop();
    delay(50);

}

void rotary_onButtonClick()
{ 

    if(on_Button_down){
      
      static unsigned long lastTimePressed = 0;
      if (millis() - lastTimePressed < 500)
          return; //ignore multiple press in that time milliseconds
        lastTimePressed = millis();

      buzzer();
      Serial.print(" buzzer");

      // rotaryEncoder.setBoundaries(-test_limits, test_limits, false);
      // Serial.print("new boundaries are between minimumn value ");
      // Serial.print(-test_limits);
      // Serial.print(" and maximum value");
      // Serial.println(-test_limits);
      // rotaryEncoder.reset();

      // if (test_limits >= 2048)
      //     test_limits = 2;
      // test_limits *= 2;
      
      on_Button_down = false;
    }
}

void rotary_loop()
{
    rotary_onButtonClick();
    //lets see if anything changed
    int16_t encoderDelta = rotaryEncoder.encoderChanged();

    //optionally we can ignore whenever there is no change
    if (encoderDelta == 0) {
      return;
    } else {
      hue += encoderDelta;
    }
    //for some cases we only want to know if value is increased or decreased (typically for menu items)
    if (encoderDelta > 0) {
        Serial.print("+");
    } else if (encoderDelta < 0) {
         Serial.print("-");
    }
       

    //for other cases we want to know what is current value. Additionally often we only want if something changed
    //example: when using rotary encoder to set termostat temperature, or sound volume etc

    //if value is changed compared to our last read
    if (encoderDelta != 0)
    {
        //now we need current value
        int16_t encoderValue = rotaryEncoder.readEncoder();
        //process new value. Here is simple output.
        Serial.print("Value: ");
        Serial.println(encoderValue);
        setLedHue((double) encoderValue * 0.5);
        delay(10);
    }
}

void setLedHSV(hsv HSV) {
  rgb RGB = hsv2rgb(HSV);
  Serial.print("RGB: ");
  Serial.print(RGB.r);
  Serial.print(", ");
  Serial.print(RGB.g);
  Serial.print(", ");
  Serial.println(RGB.b);
  ledcWrite(LEDRED_CHANNEL, (int)(RGB.r * 255.0));
  ledcWrite(LEDGREEN_CHANNEL, (int)(RGB.g * 255.0));
  ledcWrite(LEDBLUE_CHANNEL, (int)(RGB.b * 255.0));
}

/// hue from 0 - 359
void setLedHue(double hue) {
  hsv HSV;
  HSV.h = hue;
  HSV.s = 1.0;
  HSV.v = 1.0;

  setLedHSV(HSV);
}

int buzzer(){

    ledcWriteTone(BUZZER_CHANNEL, LEDC_BASE_FREQ);
    ledcWrite(BUZZER_CHANNEL, 150);
    delay(300);
    ledcWrite(BUZZER_CHANNEL, 0);

    return 1;
}

int led_red(){
  digitalWrite(LED_RED, LOW); 
  digitalWrite(LED_GREEN, HIGH); 
  digitalWrite(LED_BLUE, HIGH); 
  return 1;
}
int led_green(){
  digitalWrite(LED_RED, HIGH); 
  digitalWrite(LED_GREEN, LOW); 
  digitalWrite(LED_BLUE, HIGH);
  return 1;
}

int led_blue(){
  digitalWrite(LED_RED, HIGH); 
  digitalWrite(LED_GREEN, HIGH); 
  digitalWrite(LED_BLUE, LOW); 
  return 1;
}

rgb hsv2rgb(hsv HSV)
{
    rgb RGB;
    double H = HSV.h, S = HSV.s, V = HSV.v,
            P, Q, T,
            fract;

    (H == 360.)?(H = 0.):(H /= 60.);
    fract = H - floor(H);

    P = V*(1. - S);
    Q = V*(1. - S*fract);
    T = V*(1. - S*(1. - fract));

    if      (0. <= H && H < 1.)
        RGB = (rgb){.r = V, .g = T, .b = P};
    else if (1. <= H && H < 2.)
        RGB = (rgb){.r = Q, .g = V, .b = P};
    else if (2. <= H && H < 3.)
        RGB = (rgb){.r = P, .g = V, .b = T};
    else if (3. <= H && H < 4.)
        RGB = (rgb){.r = P, .g = Q, .b = V};
    else if (4. <= H && H < 5.)
        RGB = (rgb){.r = T, .g = P, .b = V};
    else if (5. <= H && H < 6.)
        RGB = (rgb){.r = V, .g = P, .b = Q};
    else
        RGB = (rgb){.r = 0., .g = 0., .b = 0.};

    return RGB;
}